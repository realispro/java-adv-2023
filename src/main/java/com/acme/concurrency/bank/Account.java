package com.acme.concurrency.bank;

import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger amount;

    public Account(int amount) {
        this.amount = new AtomicInteger(amount);
    }

    public void deposit(int value){
        amount.addAndGet(value);
        //amount = amount + value; // 1.read current value 2.calculate new value 3.assign new value
    }

    public void withdraw(int value){
        amount.addAndGet(-value);
        //amount = amount - value;
    }

    public int getAmount() {
        return amount.get();
    }


}
