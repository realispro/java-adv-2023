package com.acme.concurrency.alarm;


import com.acme.util.ThreadNamePrefixPrintStream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class AlarmStarter {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(2);

        List<Alarm> alarms = new ArrayList<>();
        alarms.add(new Beeper());
        alarms.add(new Flash());

        List<Future<Boolean>> futures = new ArrayList<>();

        alarms.forEach(a->futures.add(es.submit((Callable<Boolean>) a)));

        es.shutdown();

        futures.forEach(future-> {
            try {
                System.out.println("future: " + future.get());
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.println("done.");
}
}
