package com.acme.space;

import java.util.ArrayList;
import java.util.List;

/**
 * Space object type for a star
 */
public class Star extends SpaceObject{

    /**
     *
     */
    private List<Planet> planets = new ArrayList<>();

    public Star(String name, double diameter, double mass) {
        super(name, diameter, mass);
    }

    /**
     * provides planets surrounding this star
     * @return planet list
     */
    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    @Override
    public String toString() {
        return super.toString() + " planets=" + planets;
    }
}
