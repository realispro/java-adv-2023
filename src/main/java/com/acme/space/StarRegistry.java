package com.acme.space;

import com.acme.space.stars.RedSuperGiant;
import com.acme.space.stars.YellowDwarf;

import java.util.ArrayList;
import java.util.List;

public class StarRegistry  {

    private List<SpaceObject> content = new ArrayList<>();


    {
        Star sun = new YellowDwarf("Sun", 109, 332_837);

        Planet mercury = new Planet("Mercury", 0.4, 0.0553, sun);
        Planet venus = new Planet("Venus", 0.9, 0.814, sun);
        Planet earth = new Planet("Earth", 1, 1, sun);
        Planet mars = new Planet("Mars", 0.5, 0.107, sun);
        Planet jupiter = new Planet("Jupiter", 11.2, 318.72, sun);
        Planet saturn = new Planet("Saturn", 9.4, 95.15, sun);
        Planet uran = new Planet("Uran", 4, 14.53, sun);
        Planet neptun = new Planet("Neptune", 3.8, 17.14, sun);

        new Satellite("Moon", 0.2, 0.0123, earth);

        new Satellite("Phobos", 0.0002, 0.0000000018, mars);
        new Satellite("Deimos", 0.0001, 0.0000000003, mars);

        new Satellite("Io", 0.25, 0.015, jupiter);
        new Satellite("Europa", 0.2, 0.00803, jupiter);
        new Satellite("Ganimedes", 0.41, 0.024, jupiter);
        new Satellite("Callisto", 0.39, 0.018, jupiter);

        new Satellite("Titan", 0.4, 0.0225, saturn);
        new Satellite("Japet", 0.1, 0.00033, saturn);
        new Satellite("Dione", 0.8, 0.00018, saturn);

        new Satellite("Titania", 0.15, 0.00059, uran);
        new Satellite("Oberon", 0.15, 0.00050, uran);

        new Satellite("Triton", 0.19, 0.00359, neptun);


        Star betelgeza = new RedSuperGiant("Betelgeza", 1209, 5_991_066);

        Planet tatooine = new Planet("Tatooine", 3.3, 18.0, betelgeza);
        Planet melmak = new Planet("Melmak", 1, 1, betelgeza);

        new Satellite("A", 0.2, 0.04, tatooine);
        new Satellite("B", 0.0002, 0.00000001, tatooine);

        new Satellite("X", 0.25, 0.06, melmak);
        new Satellite("Y", 0.2, 0.04,  melmak);
        new Satellite("Z", 0.41, 0.1, melmak);

        register(sun);
        register(betelgeza);
    }

    public List<SpaceObject> getContent() {
        return content;
    }

    public void register(SpaceObject t) {
        content.add(t);
    }

}
