package com.acme.space;

public class Satellite extends SpaceObject{

    private Planet planet;

    public Satellite(String name, double diameter, double mass, Planet planet) {
        super(name, diameter, mass);
        this.planet = planet;
        planet.getSatellites().add(this);

    }

    public Satellite(String name, double diameter, double mass){
        super(name, diameter, mass);
    }

    public Planet getPlanet() {
        return planet;
    }
}
