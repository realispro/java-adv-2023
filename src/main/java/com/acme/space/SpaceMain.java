package com.acme.space;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpaceMain {

    public static void main(String[] args) {
        System.out.println("Let's imagine!");

        StarRegistry registry = new StarRegistry();

        List<SpaceObject> stars = registry.getContent();

        // TODO 1: list all space objects
        // TODO 2: total satellites for each star
        //todo2(stars);
        // TODO 3: ** count satellites
        //todo3(stars);
        // TODO 4: planet having the most moons
        // TODO 5: ** all planets lighter than Earth
        //todo5(stars);
        // TODO 6: all satellites heavier that Moon
        // TODO 7: ** total mass of all object for each star
        //todo7(stars);
        // TODO 8: average diameter of a planet

        //  use Stream API
        // TODO 101: list all space objects
        //todo101(stars);
        // TODO 102: count satellites
        //todo102(stars);
        // TODO 103: find planet having the heaviest moon
        //todo103(stars);
        // TODO 104: find planets heavier than Earth
       // todo104(stars);
        // TODO 105: find satellites lighter that Moon
        todo105(stars);
        // TODO 106: calculate total mass of all object for each star
        //todo106(stars);
        // TODO 107: calculate average diameter of a satellite
        //todo107(stars);

        System.out.println("planetary systems: " + stars);
    }

    public static void todo105(List<SpaceObject> spaceObjects){

        // v1 by Krzysiek
        List<Satellite> satellites = spaceObjects.stream()
                .filter(so->so instanceof Star)
                .map(so->(Star)so)
                .flatMap(star -> star.getPlanets().stream().flatMap(planet -> planet .getSatellites().stream()))
                .toList();

        Satellite moon = satellites.stream()
                .filter(satellite -> satellite.getName().equals("Moon"))
                .findAny()
                .get();

        var lighterThanMoon = satellites.stream()
                .filter(satellite -> satellite.getMass() < moon.getMass())
                .map(satellite -> satellite.getName())
                .toList();

        System.out.println("Satellites lighter than moon = " + lighterThanMoon);

        //v2 by Piotr
        List<Star> stars = spaceObjects.stream()
                .filter(so->so instanceof Star)
                .map(so->(Star)so)
                .toList();

        Satellite moon2 = stars.stream()
                .flatMap(star->star.getPlanets().stream())
                .flatMap(planet->planet.getSatellites().stream())
                .filter(planet->planet.getName().equals("Moon"))
                .findFirst()
                .get();

        var satelites =
                stars.stream()
                        .flatMap(star->star.getPlanets().stream())
                        .flatMap(planet->planet.getSatellites().stream())
                        .filter(satelite->satelite.getMass()<moon2.getMass())
                        .map(satellite->satellite.getName())
                        .toList();

        System.out.println("satelites lighter that Moon = " + satelites);
    }

    public static void todo106(List<SpaceObject> stars) {
        double sum = Stream.concat(stars.stream(),
                Stream.concat(
                        stars.stream().map(e -> (Star) e)
                                .flatMap( star -> star.getPlanets().stream()),
                        stars.stream().map(e -> (Star) e)
                                .flatMap(star->star.getPlanets().stream().flatMap(planet->planet.getSatellites().stream()))

                ))
                .mapToDouble(so->so.getMass())
                .sum();

        System.out.println("sum = " + sum);
        
        Map<String, Double> map = 
        stars.stream().map(s->(Star)s)
                .collect(Collectors.toMap(
                        star->star.getName(),
                        star->{
                            double sumPerStar = star.getPlanets().stream().mapToDouble(p->p.getMass()).sum();
                            sumPerStar+=star.getPlanets().stream()
                                    .flatMap(planet->planet.getSatellites().stream())
                                    .mapToDouble(s->s.getMass()).sum();
                            return sumPerStar;
                        }
                ));
        System.out.println("map = " + map);
    }

    public static void todo107(List<SpaceObject> stars) {
        //calculate average diameter of a satellite
        var result = stars.stream()
                .filter(star -> Star.class.isInstance(star))
                .map(obj -> Star.class.cast(obj))
                .flatMap(star->star.getPlanets().stream().flatMap(planet->planet.getSatellites().stream()))
                .mapToDouble(satellite->satellite.getDiameter())
                .average();
        System.out.println("average diameter of a satellite = " + result.getAsDouble());

    }
    
    public static void todo104(List<SpaceObject> spaceObjects){
        List<Star> stars = spaceObjects.stream()
                .filter(so->so instanceof Star)
                .map(so->(Star)so)
                .toList();
        
        Planet earth = stars.stream()
                .flatMap(star->star.getPlanets().stream())
                .filter(planet->planet.getName().equals("Earth"))
                .findAny()
                .get();

        var planets = 
        stars.stream()
                .flatMap(star->star.getPlanets().stream())
                .filter(planet->planet.getMass()>earth.getMass())
                .map(planet->planet.getName())
                .toList();
        System.out.println("planets = " + planets);
    }

        // TODO 101: list all space objects
        // TODO 102: calculate total satellites for each star
        // TODO 103: count satellites
        // TODO 104: find planet having the heaviest moon
        // TODO 105: find planets heavier than Earth
        // TODO 106: find satellites lighter that Moon
        // TODO 107: calculate total mass of all object for each star
        // TODO 108: calculate average diameter of a satellite


    public static void todo101(List<SpaceObject> stars) {
        // v1 by Piotr
        var spaceObjects = Stream.concat(stars.stream(),
                Stream.concat(
                        stars.stream().map(e -> (Star) e)
                                        .flatMap( star -> star.getPlanets().stream()),
                                //.map(Star::getPlanets).flatMap(Collection::stream),
                        stars.stream().map(e -> (Star) e)
                                .flatMap(star->star.getPlanets().stream().flatMap(planet->planet.getSatellites().stream()))
                                //.map(Star::getPlanets).flatMap(Collection::stream)
                                //.map(Planet::getSatellites).flatMap(Collection::stream)
                )).toList();
        for (var spaceObject : spaceObjects) {
            System.out.println(spaceObject);
        }

        // v2 by Stanislaw
        var spaceObjectsList = new ArrayList<String>();
        stars.stream()
                .filter(s -> s instanceof Star)
                .flatMap(s -> {
                    Star star = (Star) s;
                    spaceObjectsList.add(star.getName());
                    return star.getPlanets().stream();
                })
                .flatMap(planet -> {
                    spaceObjectsList.add(planet.getName());
                    return planet.getSatellites().stream();
                })
                .forEach(satellite -> spaceObjectsList.add(satellite.getName()));

        System.out.println("spaceObjectsList " + spaceObjectsList);

    }

    public static void todo102(List<SpaceObject> stars) {
        var satCount = stars.stream()
                .map(o-> (Star)o)
                .flatMap(s->s.getPlanets().stream().flatMap(planet->planet.getSatellites().stream()))
                .count();
        System.out.println("satellite count: " + satCount);
    }


    public static void todo103(List<SpaceObject> spaceObjects){

        // v1 by Sebastian
        Planet planet = spaceObjects.stream()
                .filter(spaceObject -> spaceObject instanceof Star)
                .map(spaceObject -> (Star) spaceObject)
                .flatMap(star -> star.getPlanets().stream())
                .sorted(SpaceMain::comaparePlanetSateliteMass)
                .findFirst().get();

        System.out.println(planet);

        // v2 by Krzysiek
        Optional<Satellite> heaviestMoon = spaceObjects.stream()
                .filter(so->so instanceof Star)
                .map(so -> (Star) so)
                .flatMap(star -> star.getPlanets().stream().flatMap(p->p.getSatellites().stream()))
                .max(Comparator.comparing(Satellite::getMass));

        System.out.println("Planet with heaviest moon: " + heaviestMoon.orElseThrow().getPlanet().getName());
    }

    public static int comaparePlanetSateliteMass(Planet planet1, Planet planet2){
        double result =
                planet2.getSatellites().stream()
                        .map(SpaceObject::getMass)
                        .sorted((m1, m2)-> m2>m1 ? 1 : -1)
                        .findFirst()
                        .orElseGet(() -> 0.0)
                        -
                planet1.getSatellites().stream()
                        .map(SpaceObject::getMass)
                        .sorted((m1, m2)-> m2>m1 ? 1 : -1)
                        .findFirst()
                        .orElseGet(() -> 0.0);

        if (result == 0) return 0;
        if (result > 0) return 1;
        return -1;
    }


    public static void todo2(List<SpaceObject> stars) {

        Map<String, Integer> map = new HashMap<>();

        for (SpaceObject so : stars) {
            if (so instanceof Star star) {
                int count = 0;
                for (Planet planet : star.getPlanets()) {
                    count += planet.getSatellites().size();
                }
                map.put(star.getName(), count);
            }
        }
        System.out.println("map = " + map);
    }

    public static void todo3(List<SpaceObject> stars) {
        var numOfSatelitsInGalaxy = 0;
        for (SpaceObject star : stars) {
            if (star instanceof Star realStar) {
                //var realStar = (Star) star; // TODO ensure type casting
                var planets = realStar.getPlanets();
                for (Planet planet : planets) {
                    var numOfSatelits = planet.getSatellites().size();
                    numOfSatelitsInGalaxy += numOfSatelits;
                }
            }
        }

        System.out.println(" todo3 Liczba satelit: " + numOfSatelitsInGalaxy);
    }


    public static void todo5(List<SpaceObject> spaceObjects) {
        List<Star> stars = new ArrayList<>();
        for (SpaceObject so : spaceObjects) {
            if (so instanceof Star) stars.add((Star) so);
        }

        List<Planet> planets = new ArrayList<>();
        for (Star star : stars) {
            planets.addAll(star.getPlanets());
        }

        List<Planet> lighterPlanets = new ArrayList<>();

        for (Planet planet : planets) {
            if (planet.getMass() < 1) {
                lighterPlanets.add(planet);
            }
        }
        System.out.println("lighterPlanets = " + lighterPlanets);
    }

    public static void todo7(List<SpaceObject> stars) {
        double totalMass = 0;
        for (SpaceObject star : stars) {
            Star starT = (Star) star;
            totalMass = totalMass + starT.getMass();

            for (Planet planet : starT.getPlanets()) {
                totalMass = totalMass + planet.getMass();

                for (Satellite satellite : planet.getSatellites()) {
                    totalMass = totalMass + satellite.getMass();
                }
            }
        }

        System.out.println("totalMass = " + totalMass);
    }

}
