package com.acme.space;

import com.acme.reflect.Init;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode
public class Planet extends SpaceObject{

    private Star star; // void setStar(Star s)

    private List<Satellite> satellites = new ArrayList<>();

    private String firstwordSecondword;

    public Planet(String name, double diameter, double mass, Star star) throws NullPointerException{
        super(name, diameter, mass);
        this.star = star;
        star.getPlanets().add(this);
    }

    private Planet(String name, double diameter, double mass){
        super(name, diameter, mass);
    }

    @Init("planet initializer")
    public void init(){
        System.out.println("init: planet initialization: " + this);
    }

    public Star getStar() {
        return star;
    }

    public List<Satellite> getSatellites() {
        return satellites;
    }

    public void setStar(Star star) {
        this.star = star;
    }

    public void setSatellites(List<Satellite> satellites) {
        this.satellites = satellites;
    }

    @Override
    public String toString() {
        return super.toString() + " satellites=" + satellites;
    }
}
