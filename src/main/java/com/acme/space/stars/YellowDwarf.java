package com.acme.space.stars;

import com.acme.space.Star;

public class YellowDwarf extends Star {
    public YellowDwarf(String name, double diameter, double mass) {
        super(name, diameter, mass);
    }
}
