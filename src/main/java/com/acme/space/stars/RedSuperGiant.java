package com.acme.space.stars;

import com.acme.space.Star;

public class RedSuperGiant extends Star {
    public RedSuperGiant(String name, double diameter, double mass) {
        super(name, diameter, mass);
    }
}
