package com.acme.space;


import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;


/**
 * The class representing generic object which is floating in the space.
 */
@NoArgsConstructor
public abstract class SpaceObject {

    @NotNull
    private String name;
    @Positive
    private double diameter;
    @Positive
    private double mass;

    /**
     * Constructor creating space object instance and setting basic fields.
     *
     * @param name the name of the object
     * @param diameter diameter of the object, must be positive, throws otherwise
     * @param mass mass of the object
     * @throws IllegalArgumentException if diameter is negative
     */
    public SpaceObject(String name, double diameter, double mass) throws IllegalArgumentException{
        if(diameter<0){
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.diameter = diameter;
        this.mass = mass;
    }

    /**
     * Return name of the object
     * @return name of the object
     */
    public String getName() {
        return name;
    }

    public double getDiameter() {
        return diameter;
    }

    public double getMass() {
        return mass;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name +
                ", diameter=" + diameter +
                ", mass=" + mass +
                '}';
    }
}
