package com.acme.reflect;

import com.acme.space.Planet;
import com.acme.space.SpaceObject;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Set;

public class ReflectionMain {

    public static void main(String[] args) {

        String spaceObjectClassName = "com.acme.space.Planet";
        String name = "XYZ";
        double diameter = 123.456;
        double mass = 1.23;

        try {
            Class clazz = Class.forName(spaceObjectClassName);
            Constructor constructor = clazz.getDeclaredConstructor(String.class, double.class, double.class);
            constructor.setAccessible(true);
            Object object = constructor.newInstance(name, diameter, mass);
            if(object instanceof SpaceObject spaceObject){

                Arrays.stream(spaceObject.getClass().getDeclaredMethods())
                        .filter(m->m.isAnnotationPresent(Init.class))
                        .filter(m->m.getReturnType()==void.class && m.getParameterCount()==0)
                        .forEach(m->{
                            Init init = m.getAnnotation(Init.class);
                            System.out.println("Init annotation processing: " + init.value());
                            try {
                                m.invoke(spaceObject);
                            } catch (IllegalAccessException e) {
                                throw new RuntimeException(e);
                            } catch (InvocationTargetException e) {
                                throw new RuntimeException(e);
                            }
                        });


                Field field = getField(clazz, "diameter");
                field.setAccessible(true);
                field.set(spaceObject, -spaceObject.getDiameter());

                ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
                Validator validator = factory.getValidator();
                Set<ConstraintViolation<SpaceObject>> violations = validator.validate(spaceObject);

                if(!violations.isEmpty()){
                    violations.forEach(v-> System.out.println("violation detected: " + v));
                }

                System.out.println("space object created: " + spaceObject);

            } else {
                throw new IllegalStateException("unexpected object type: " + object.getClass().getName());
            }

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static Field getField(Class clazz, String fieldName){
        do{
            try {
                Field field = clazz.getDeclaredField(fieldName);
                return field;
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }while(clazz!= Object.class);
        return null;
    }
}
