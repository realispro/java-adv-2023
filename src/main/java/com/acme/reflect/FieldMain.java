package com.acme.reflect;

import com.acme.development.Role;
import com.acme.development.Skill;
import com.acme.development.TeamMember;
import lombok.Getter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FieldMain {

    static List<SetterInfo> setterInfos = new ArrayList<>();

    public static void main(String[] args) {

        Object object = new TeamMember(null)
                .withSkill(Skill.JAVA)
                .withRole(Role.DEVELOPER);

        Class clazz = object.getClass();

        setterInfos = Arrays.stream(clazz.getDeclaredFields())
                .map(field -> new SetterInfo(field.getName(), field.getType()))
                .collect(Collectors.toList());

        setterInfos.forEach(setter -> {
            try {
                Method method = clazz.getDeclaredMethod(setter.setterName, setter.type);
                if (Modifier.isPublic(method.getModifiers()) && method.getReturnType() == void.class) {
                    return;
                }
            } catch (NoSuchMethodException e) {
                // no setter found
            }

            Field field = getField(clazz, setter.name);
            field.setAccessible(true);
            try {
                Object value = field.get(object);
                if(value==null){
                    System.out.println("no setter and value for field " + setter.name);
                    DefaultValue annotation = field.getAnnotation(DefaultValue.class);
                    if (annotation != null && field.getType().equals(annotation.value().getClass())) {
                        field.set(object, annotation.value());
                    }
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.println("Object: " + object);



    }

    public static Field getField(Class clazz, String fieldName){
        do{
            try {
                Field field = clazz.getDeclaredField(fieldName);
                return field;
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }while(clazz!= Object.class);
        return null;
    }


    @Getter
    public static class SetterInfo{
        private String name;
        private String setterName;
        private Class type;

        public SetterInfo(String name, Class type) {
            this.name = name;
            this.setterName = "set" + name.substring(0,1).toUpperCase() + name.substring(1);
            this.type = type;
        }
    }
}
