package com.acme.reflect;

import com.acme.space.Planet;
import com.acme.space.Star;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ClassViewerMain {

    public static void main(String[] args) {

        Class clazz = getClazz();

        do {
            ClassViewer viewer = new ClassViewer(clazz);
            viewer.printClassInfo();
            viewer.printFieldsInfo();
            viewer.printConstructorInfo();
            viewer.printMethodInfo();
            clazz = clazz.getSuperclass();
        }while (clazz!=Object.class);
    }

    public static Class getClazz(){
        //try {
            Object object = new Planet(
                    "X",
                    1,
                    1,
                    new Star("Z", 1000, 1000));
            return object.getClass();
                    //Class.forName("com.acme.space.Planet");
                    //Planet.class;
        /*} catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }*/

    }

    @RequiredArgsConstructor
    static class ClassViewer {

        private final Class clazz;

        public void printClassInfo(){
            System.out.println("--------------------------------------------");
            System.out.println("Class name: " + clazz.getSimpleName());
            System.out.println("Package name: " + clazz.getPackageName());
            System.out.println("Interfaces: " + Arrays.toString(clazz.getInterfaces()));
            System.out.println("Modifiers: " + formatModifiers(clazz.getModifiers()));
        }


        public void printFieldsInfo(){
            Arrays.stream(clazz.getDeclaredFields())
                    .forEach(field -> {
                        System.out.println("field name: " + field.getName());
                        System.out.println("field modifiers: " + formatModifiers(field.getModifiers()));
                        System.out.println("field type: " + field.getType().getSimpleName());
                    });
        }

        public void printConstructorInfo(){
            Arrays.stream(clazz.getDeclaredConstructors())
                    .forEach(c->{
                        System.out.println("constructor params: " + Arrays.toString(c.getParameterTypes()));
                        System.out.println("constructor modifiers: " + formatModifiers(c.getModifiers()));
                        System.out.println("constructor exceptions: " + Arrays.toString(c.getExceptionTypes()));
                    });
        }

        public void printMethodInfo(){
            Arrays.stream(clazz.getDeclaredMethods())
                    .forEach(m -> {
                        System.out.println("method name: " + m.getName());
                        System.out.println("method params: " + Arrays.toString(m.getParameterTypes()));
                        System.out.println("method return type: " + m.getReturnType().getSimpleName());
                        System.out.println("method modifiers: " + formatModifiers(m.getModifiers()));
                    });
        }


        private String formatModifiers(int modifiers){
            StringBuilder builder = new StringBuilder();
            builder.append("public=").append(Modifier.isPublic(modifiers));
            builder.append(",protected=").append(Modifier.isProtected(modifiers));
            builder.append(",private=").append(Modifier.isPrivate(modifiers));
            builder.append(",abstract=").append(Modifier.isAbstract(modifiers));
            builder.append(",final=").append(Modifier.isFinal(modifiers));
            builder.append(",static=").append(Modifier.isStatic(modifiers));
            builder.append(",native=").append(Modifier.isNative(modifiers));
            return builder.toString();
        }



    }
}
