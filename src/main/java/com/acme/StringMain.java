package com.acme;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringMain {

    public static void main(String[] args) {

        String s1 = "Summer";
        String s2 = new String("Summer");
        String s3 = "Summer";

        char c = 'S';

        s2 = null;

        System.out.println("s1==s2: " + (s1 == s2));
        System.out.println("s1==s3: " + (s1 == s3));
        System.out.println("s2==s3: " + (s2 == s3));

        String names = "Kowal,Kowalski,Kowalewski";
        String[] namesArray = names.split(",");
        System.out.println("names: " + Arrays.toString(namesArray));

        //String namesJoined = "";
        StringBuilder builder = new StringBuilder();
        for (String name : namesArray) {
            builder.append("$").append(name);
            //namesJoined = namesJoined + "$" + name;
        }
        System.out.println("namesJoined = " + builder.toString());


        String text = "00-950 Warszawa, 30512 Krakow, 88 100 Torun";
        String patternText = "\\d{2}[\\s-]?\\d{3}";

        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(text);

        while(matcher.find()){
            System.out.println("found: start=" + matcher.start() + " end=" + matcher.end() + " group=" + matcher.group());
        }



        System.out.println("done.");


    }
}
