package com.acme;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeMain {

    public static void main(String[] args) {

        // legacy
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();

        //calendar.setTime(date);
        calendar.set(2023, 5, 22,9, 35, 0);
        calendar.add(Calendar.MONTH, 8);

        date = calendar.getTime();

        Locale locale = new Locale("pl", "PL");
                //Locale.ITALY;
                //Locale.getDefault();
        DateFormat format = new SimpleDateFormat("w W D E yyyy!MM!dd hh!mm!ss!SS G X", locale);
                //DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, locale);
        String timestamp = format.format(date);
        System.out.println("[legacy] timestamp = " + timestamp);

        // java.time

        LocalDate ld = LocalDate.now().plusMonths(8);
        YearMonth ym = YearMonth.from(ld);
        LocalDateTime ldt = LocalDateTime.of(2023, 6, 22, 9, 51, 0, 0);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("w W D E yyyy!MM!dd hh!mm!ss!SS G", locale);
        timestamp = formatter.format(ldt);
        System.out.println("[java.time] timestamp = " + timestamp);

        LocalDateTime now = LocalDateTime.now();

        ZoneId warsawZone = ZoneId.of("Europe/Warsaw");
        ZonedDateTime warsawStart = ZonedDateTime.of(now, warsawZone);

        ZonedDateTime warsawLanding = warsawStart.plusHours(10);
        ZoneId singaporeZone = ZoneId.of("Asia/Singapore");
        ZonedDateTime singaporeLanding = warsawLanding.withZoneSameInstant(singaporeZone);

        System.out.println("warsawLanding = " + warsawLanding);
        System.out.println("singaporeLanding = " + singaporeLanding);

        // DST
        LocalDateTime localDateTimeBeforeDST = LocalDateTime.of(2018, 3, 25, 1, 55);
        System.out.println("localDateTimeBeforeDST = " + localDateTimeBeforeDST);
        //assertThat(localDateTimeBeforeDST.toString()).isEqualTo("2018-03-25T01:55");

        ZoneId italianZoneId = ZoneId.of("Europe/Rome");
        ZonedDateTime zonedDateTimeBeforeDST = localDateTimeBeforeDST.atZone(italianZoneId);
        System.out.println("zonedDateTimeBeforeDST = " + zonedDateTimeBeforeDST);
        //assertThat(zonedDateTimeBeforeDST.toString()).isEqualTo("2018-03-25T01:55+01:00[Europe/Rome]");

        ZonedDateTime zonedDateTimeAfterDST = zonedDateTimeBeforeDST.plus(10, ChronoUnit.MINUTES);
        System.out.println("zonedDateTimeAfterDST = " + zonedDateTimeAfterDST);
        //assertThat(zonedDateTimeAfterDST.toString()).isEqualTo("2018-03-25T03:05+02:00[Europe/Rome]");

        Long deltaBetweenDatesInMinutes = ChronoUnit.MINUTES.between(zonedDateTimeBeforeDST,zonedDateTimeAfterDST);
        //assertThat(deltaBetweenDatesInMinutes).isEqualTo(10);
        System.out.println("deltaBetweenDatesInMinutes = " + deltaBetweenDatesInMinutes);
    }
}
