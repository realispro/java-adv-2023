package com.acme.generics.box;

public class GenericsMain {


    public static void main(String[] args) {
        System.out.println("Let's generalize!");

        Box<Integer> box = new IntegerBox();
                //new NumberBox<>();
                //new Box<>();
        box.setContent(123);
        Integer i = box.getContent();
        System.out.println("i = " + i);
        
    }
}
