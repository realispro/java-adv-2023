package com.acme.generics.product;


import java.util.List;

public class Book extends Product {

    private final String title;
    private final List<String> authors;

    public Book(String title, List<String> authors) {
        this.title = title;
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    @Override
    public String getName() {
        return title + " : " + authors;
    }
}
