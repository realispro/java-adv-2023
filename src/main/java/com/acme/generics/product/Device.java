package com.acme.generics.product;

public class Device extends Product {

    private final String producer;
    private final String model;

    public Device(String producer, String model) {
        this.producer = producer;
        this.model = model;
    }

    public String getProducer() {
        return producer;
    }

    public String getModel() {
        return model;
    }

    @Override
    public String getName() {
        return producer + ":" + model;
    }
}
