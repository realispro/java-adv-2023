package com.acme.generics.product;

import java.util.List;

public class ProductMain {

    public static void main(String[] args) {
        System.out.println("Let's buy something!");

        Cart<Device> cart = new Cart<>();
        //cart.add(new Book("Thinking in Java", List.of("Bruce Eckhell")));
        cart.add(new Device("t-link", "123"));
        cart.add(new Mobile("IPone", "24", 512));  

        Cart<String> cart2 = new Cart<>();

        Cart<Product> cart3 = new Cart<>();

        addGratis(cart3);

        for (String s : cart2.getElements()) {
            System.out.println("s = " + s);
        }



        System.out.println("cart = " + cart2);
        

    }
    
    public static void addGratis(Cart<? super Device> cart){
    
        cart.add(new Device("Huawei", "XYZ123"));
        
    }





}
