package com.acme.generics.product;

public class Mobile extends Device{

    private final int memory;

    public Mobile(String producer, String model, int memory) {
        super(producer, model);
        this.memory = memory;
    }

    public int getMemory() {
        return memory;
    }
}
