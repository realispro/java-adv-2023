package com.acme.generics.product;

public abstract class Product{

    protected float price;

    public abstract String getName();

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getType() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return "Product{" +
                "type=" + getType() + "," +
                "name=" + getName() + "," +
                "price=" + price + "," +
                '}';
    }
}
