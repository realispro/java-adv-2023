package com.acme.generics.product;

import java.util.ArrayList;
import java.util.List;

public class Cart<E>  {

    private List<E> elements = new ArrayList<>();

     void add(E product){
        System.out.println("adding product to the cart: " + product);
        elements.add(product);
    }

    public void remove(E product){
        elements.remove(product);
    }

    public List<E> getElements(){
         return elements;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "elements=" + elements +
                '}';
    }
}
