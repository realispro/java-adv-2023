package com.acme.travel;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class TravelMain {

    public static void main(String[] args) {
        System.out.println("Let's travel");

        String traveller = "Jan Kowalski";

        Transportation transportation = getTransportation();
        System.out.println("starting transport, speed: " + transportation.getSpeed());
        transportation.transport(traveller);

        System.out.println("done. " + Transportation.getDescription());
    }

    public static Transportation getTransportation(){

        Transportation transportation = new Bus();

        InvocationHandler invocationHandler = new TimeMeasureInvocationHandler(transportation);
        transportation = (Transportation)Proxy.newProxyInstance(
               Transportation.class.getClassLoader(),
               new Class[]{Transportation.class},
               invocationHandler
        );

        invocationHandler = new TimeWindowInvocationHandler(transportation, 9, 16);
        transportation = (Transportation)Proxy.newProxyInstance(
                Transportation.class.getClassLoader(),
                new Class[]{Transportation.class},
                invocationHandler
        );

        return transportation;
                //System.out::println;
                // = passenger -> System.out.println(passenger);
                //;
    }
}
