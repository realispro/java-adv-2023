package com.acme.travel;

@FunctionalInterface
public interface Transportation {

    void transport(String passenger);

    default int getSpeed(){
        return -1*factor();
    }

    static String getDescription(){
        return "Object used for transport function";
    }

    private int factor(){
        return 10;
    }
}
