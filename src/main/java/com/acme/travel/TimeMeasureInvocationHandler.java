package com.acme.travel;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

@RequiredArgsConstructor
public class TimeMeasureInvocationHandler implements InvocationHandler {

    private final Transportation transportation;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long start = System.currentTimeMillis();
        Object result = method.invoke(transportation, args);
        long end = System.currentTimeMillis();
        System.out.println("method " + method.getName() + " execution took " + (end-start) + " millis");

        return result;
    }
}
