package com.acme.travel;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.time.OffsetDateTime;

@RequiredArgsConstructor
public class TimeWindowInvocationHandler implements InvocationHandler {

    private final Transportation transportation;
    private final int from;
    private final int to;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        OffsetDateTime now = OffsetDateTime.now();
        int hour = now.getHour();
        if (from <= hour && to >= hour) {
            System.out.println("method " + method.getName() + " invoked at " + hour + " successfully processed");
            return method.invoke(transportation, args);
        } else {
            System.out.println("method " + method.getName() + " invoked at " + hour);
            throw new RuntimeException("outside time window");
        }
    }
}
