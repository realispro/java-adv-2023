package com.acme.travel;

public class Bus implements Transportation{
    @Override
    public void transport(String passenger) {
        System.out.println("Transporting passenger " + passenger + " by bus");
    }

    @Override
    public int getSpeed() {
        return 90;
    }
}
