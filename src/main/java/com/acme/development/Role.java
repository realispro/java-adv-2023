package com.acme.development;

public enum Role {

    DEVELOPER,
    QA,
    PRODUCT_OWNER,
    SCRUM_MASTER,
    TECHNICAL_WRITER

}
