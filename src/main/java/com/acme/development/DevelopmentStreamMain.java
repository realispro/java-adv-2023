package com.acme.development;

import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DevelopmentStreamMain {

    public static void main(String[] args) {

        Project project = DevelopmentMain.constructTeam("JSystems project");

        List<TeamMember> memberList = project.getMembers();

        Stream<TeamMember> stream = memberList.parallelStream();

        if(stream.isParallel()){
            stream = stream.sequential();
        }

        var result = stream
                // ***** intermediate ops *************
                .filter(m -> m.getSkills().contains(Skill.JAVA))
                .sorted( (m1, m2) -> m1.getName().compareTo(m2.getName()))
                //.limit(2)
                .distinct()
                .map(m->m.getName())
                // ***** terminal op ********
                //.count();
                //.collect(Collectors.toList()); // .toList() J16
                .anyMatch(n->n.contains("a"));

        System.out.println("result = " + result);

        var skills = memberList.stream()
                .flatMap(m->m.getSkills().stream())
                .collect(Collectors.toSet());
                //.distinct()
                //.toList();
        System.out.println("skills = " + skills);


        var result2 =
        memberList.stream()
                .filter(m->m.getSkills().contains(Skill.SQL))
                .sorted((m1,m2)->m1.getName().compareTo(m2.getName()))
                .peek(m->System.out.println("sql:" + m))
                //.collect(Collectors.toList());
                //.anyMatch(m->m.getName().contains("b"));
                .findFirst();

        Supplier<RuntimeException> exceptionSupplier = () -> new IllegalArgumentException("null value");
        System.out.println("result2 = " + result2.orElseThrow(exceptionSupplier));

        Stream<Integer> integerStream = Stream.iterate(1, i -> i+1);
                //.generate(()->1);

        int sum = integerStream
                .limit(10)
                //.forEach(i->System.out.println(i));
                .mapToInt(i->i.intValue())
                //.reduce(0, (i1, i2)-> i1+i2);
                .sum();
        System.out.println("sum=" + sum);

        OptionalDouble average = IntStream
                .iterate(1, i -> i+1)
                .limit(10)
                        .average();
        System.out.println("average.orElse(0) = " + average.orElse(0));
        
        Map<String, TeamMember> map =
        memberList.stream()
                .distinct()
                .collect(Collectors.toMap(TeamMember::getName, Function.identity()));
        
        TeamMember bob = map.get("Bob");
        System.out.println("bob = " + bob);
        

    }
}
