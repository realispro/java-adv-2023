package com.acme.development;

import java.util.*;

public class Project {

    private String name;
    //private Set<TeamMember> members = new TreeSet<>();
    private List<TeamMember> members = new ArrayList<>();
            //new LinkedList<>();

    public Project(String name) {
        this.name = name;
    }

    public void addMember(TeamMember member){
        //if(!members.contains(member)) {
            members.add(member);
        //}
    }

    public void removeMember(TeamMember member){
        members.remove(member);
    }

    public String getName() {
        return name;
    }

    public List<TeamMember> getMembers() {
        return members;
    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                ", members=" + members +
                '}';
    }
}
