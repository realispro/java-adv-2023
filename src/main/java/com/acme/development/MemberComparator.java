package com.acme.development;

import java.util.Comparator;

public class MemberComparator implements Comparator<TeamMember> {
    @Override
    public int compare(TeamMember tm1, TeamMember tm2) {
        return tm1.getName().compareTo(tm2.getName());
    }
}
