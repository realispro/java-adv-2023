package com.acme.development;

import com.acme.util.ThreadNamePrefixPrintStream;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class DevelopmentMain {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        System.out.println("Let's develop!");
        Project project = constructTeam("A Team");

        List<TeamMember> members = project.getMembers();

        System.out.println("memebers count: " + members.size());

        // filtering
        //Predicate<TeamMember> predicate = tm -> tm.getRoles().contains(Role.TECHNICAL_WRITER) ;
        members.removeIf( tm -> tm.getRoles().contains(Role.TECHNICAL_WRITER) );

        // sorting
        Comparator<TeamMember> comparator = (tm1, tm2) -> tm1.getRoles().size()-tm2.getRoles().size();

                //new MemberComparator();
                /*new Comparator<TeamMember>() {
                    @Override
                    public int compare(TeamMember tm1, TeamMember tm2) {
                        return tm1.getRoles().size()-tm2.getRoles().size();
                    }
                };*/
        Collections.sort(members, (tm1, tm2) -> tm1.getRoles().size()-tm2.getRoles().size());

        // visiting
        //Consumer<TeamMember> consumer = tm -> System.out.println("member = " + tm);
        members.forEach( tm -> System.out.println("member = " + tm) );

    }

    public static Project constructTeam(String name) {
        Project team = new Project(name);
        TeamMember joe = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        TeamMember joeBis = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);
        team.addMember(joeBis);
        team.addMember(new TeamMember("Jane")
                .withSkill(Skill.ANGULAR)
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.JAVA)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER)
                .withRole(Role.SCRUM_MASTER));
        team.addMember(new TeamMember("Bob")
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.REACT_JS)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.QA));
        team.addMember(new TeamMember("Betty")
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withRole(Role.QA));

        team.addMember(joe);

        team.addMember(new TeamMember("Billy")
                .withSkill(Skill.SQL)
                .withRole(Role.TECHNICAL_WRITER)
                .withRole(Role.PRODUCT_OWNER));

        return team;
    }
}
