package com.acme.development;


import com.acme.reflect.DefaultValue;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@EqualsAndHashCode
@Getter
@ToString
//@RequiredArgsConstructor
public final class TeamMember /*implements Comparable<TeamMember>*/ {

    @DefaultValue("no name")
    private String name;

    private final Set<Skill> skills = new HashSet<>();
    private final Set<Role> roles = new HashSet<>();

    public TeamMember(String name) {
        this.name = name;
    }

    public TeamMember withSkill(Skill skill){
        skills.add(skill);
        return this;
    }

    public TeamMember withRole(Role role){
        roles.add(role);
        return this;
    }

/*    public void setName(String name) {
        this.name = name;
    }*/

    public void removeRole(Role role){
        roles.remove(role);
    }

    /*@Override
    public int compareTo(TeamMember tm) {
        return this.skills.size() - tm.getSkills().size();
    }*/
}
